Returns some meta information from the given URL.

# Commands #

- `npm install` - Install the dependencies.
- `node scripts/cli.js <url>` - Run the script.
- `npm test` - Run the tests.

# Example #

- `node scripts/cli.js http://example.com/`

Returns:

    {
        "url": "http://example.com",
        "tagsCount": { "html":1,"head":1,"title":1,"meta":3,"style":1,"body":1,"div":1,"h1":1,"p":2,"a":1},
        "links": ["http://www.iana.org/domains/example"],
        "maxDepth": 6,
        "ok": true
    }

# Return dictionary description #

    {
        ok: boolean;        // tells if it worked or not
        message?: string;   // in case of error, has a message explaining what was the issue
        url?: string;       // the URL that was used
        tagsCount?: { [tagName: string]: number; }  // the number of times each HTML tag appeared
        links?: string[];   // links from <a> tags
        maxDepth?: number;  // maximum depth of the HTML tree
    }