var program = require( 'commander' );
var main = require( './main.js' );


var URL;

program
    .version( '1.0.0' )
    .arguments( '<url>' )
    .action( function( url ) {
        URL = url;
    })
    .parse( process.argv );

    // start working on the given url
analyse( URL );


/**
 * Analyse the given url.
 */
async function analyse( url ) {
    var result = await main( url );
    printResult( result );
}


/**
 * Print to stdout the 'result' data.
 */
function printResult( result ) {
    console.log( JSON.stringify( result ) );
}