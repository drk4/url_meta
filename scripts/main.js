var request = require( 'request-promise' );
var parse5 = require( 'parse5' );


/**
 * {
 *     ok: boolean;
 *     message?: string;
 *     url?: string;
 *     tagsCount?: { [key: string]: number; }
 *     links?: string[];
 *     maxDepth?: number;
 * }
 */
var RESULT = {};


/**
 * Get the given URL and build some meta data about the content.
 */
module.exports = async function( url ) {
        // check if we got the required url
    if ( typeof url === 'undefined' ) {
        return invalidRequest( 'No URL given.' );
    }

    RESULT.url = url;

    try {
        var response = await request( url );
    }

    catch( error ) {
        return invalidRequest( `Invalid URL. ${ error.code }` );
    }

        // initialize the necessary properties
    Object.assign( RESULT, {
        tagsCount: {},
        links: [],
        maxDepth: 0,
        ok: true
    });

    var document = parse5.parse( response );

    travel( document, 0 );
    return RESULT;
}


/**
 * Travel through the node element and its children.
 */
function travel( element, depth ) {
    if ( !element ) {
        return;
    }

    var name = element.tagName;


    if ( name ) {
        name = name.toLowerCase();

        countTags( element, name );
        saveLinks( element, name );
    }

    calculateMaxDepth( depth );

    if ( element.childNodes ) {
        for (var a = 0 ; a < element.childNodes.length ; a++) {
            travel( element.childNodes[ a ], depth + 1 );
        }
    }
}


/**
 * Count the number of html tags we find.
 */
function countTags( element, name ) {
    var tagsCount = RESULT.tagsCount;

        // add the tag to the counter
    if ( typeof tagsCount[ name ] === 'undefined' ) {
        tagsCount[ name ] = 1;
    }

    else {
        tagsCount[ name ]++;
    }
}


/**
 * Save each link url to a list.
 */
function saveLinks( element, name ) {
        // check if its a link, and if so save the url
    if ( name === 'a' ) {
        var href = getAttribute( element, 'href' );

        if ( href ) {
            RESULT.links.push( href );
        }
    }
}


/**
 * Figure out the higher depth value of the HTML tree.
 */
function calculateMaxDepth( depth ) {
        // add to the depth
    if ( depth > RESULT.maxDepth ) {
        RESULT.maxDepth = depth;
    }
}


/**
 * Called when there was some issue with the program.
 */
function invalidRequest( message ) {
    Object.assign( RESULT, {
        message: message,
        ok: false
    });

    return RESULT;
}


/**
 * Returns the value of an attribute from a node (constructed from the parsed HTML).
 */
function getAttribute( node, attribute ) {
    if ( node && node.attrs && node.attrs.length > 0 ) {
        for (var a = 0 ; a < node.attrs.length ; a++) {
            var attr = node.attrs[ a ];

            if ( attr.name === 'href' ) {
                return attr.value;
            }
        }
    }
}