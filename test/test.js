var assert = require( 'assert' );
var childProcess = require( 'child_process' );
var util = require( 'util' );

var exec = util.promisify( childProcess.exec );


async function run( url ) {
    var { stdout, stderr } = await exec( `node ./scripts/cli.js ${ url }` );
    return JSON.parse( stdout );
}


describe( 'Shell script.', function() {
    it( 'Fails without an URL.', async function() {
        var output = await run();
        assert.equal( output.ok, false );
    });

    it( 'Fails with an invalid URL.', async function() {
        var url = 'http://example.com1';
        var output = await run( url );
        assert.equal( output.ok, false );
        assert.equal( output.url, url );
    });

    it( 'Fails with a non-existing page on a valid URL.', async function() {
        var url = 'http://example.com/test';
        var output = await run( url );
        assert.equal( output.ok, false );
        assert.equal( output.url, url );
    });

    it( 'Runs correctly with a valid URL.', async function() {
        var url = 'http://example.com/';
        var output = await run( url );

        assert.equal( output.tagsCount.html, 1 );
        assert.equal( output.tagsCount.head, 1 );
        assert.equal( output.tagsCount.p, 2 );
        assert.equal( output.url, url );
        assert.equal( output.maxDepth, 6 );
        assert.equal( output.links[ 0 ], 'http://www.iana.org/domains/example' );
        assert.equal( output.ok, true );
    });
});
